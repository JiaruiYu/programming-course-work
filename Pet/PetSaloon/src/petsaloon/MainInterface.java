package petsaloon;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class MainInterface {
	private Secretary m_sec = null;
	private Scanner m_scan = null;
	public MainInterface()
	{
		m_sec = Secretary.getInstance();
		m_scan = new Scanner(System.in);
	}
	
	public Secretary getSecretary()
	{
		return m_sec;
	}
	
	//initialize some data, 4 pet setter, 12 pets and 20 appointments
	public void initData()
	{
		if (null == m_sec)
			return;
		//add pet setters to secretary
		m_sec.addPetSetter(new PetSetter("pet's home #1"));
		m_sec.addPetSetter(new PetSetter("pet's home #2"));
		m_sec.addPetSetter(new PetSetter("pet's home #3"));
		m_sec.addPetSetter(new PetSetter("pet's home #4"));
		//add pets information for pet setter
		ArrayList<PetSetter> petSetters = m_sec.getPetSetters();
		Pet pet_1 = new Pet("Cookie", petSetters.get(0), "Jack");
		petSetters.get(0).addPets(pet_1);
		Pet pet_2 = new Pet("Fudge", petSetters.get(0), "Bill");
		petSetters.get(0).addPets(pet_2);
		Pet pet_3 = new Pet("Rolo", petSetters.get(0), "Lily");
		petSetters.get(0).addPets(pet_3);
		Pet pet_4 = new Pet("Oreo", petSetters.get(1), "Jim");
		petSetters.get(1).addPets(pet_4);
		Pet pet_5 = new Pet("Marley", petSetters.get(1), "Ali");
		petSetters.get(1).addPets(pet_5);
		Pet pet_6 = new Pet("Ozzy", petSetters.get(1), "Kate");
		petSetters.get(1).addPets(pet_6);
		Pet pet_7 = new Pet("Elvis", petSetters.get(1), "Katty");
		petSetters.get(1).addPets(pet_7);
		Pet pet_8 = new Pet("Bowie", petSetters.get(2), "Joffrey");
		petSetters.get(2).addPets(pet_8);
		Pet pet_9 = new Pet("Simba", petSetters.get(2), "John");
		petSetters.get(2).addPets(pet_9);
		Pet pet_10 = new Pet("Elsa", petSetters.get(3), "Tyron");
		petSetters.get(3).addPets(pet_10);
		Pet pet_11 = new Pet("Baloo", petSetters.get(3), "Tywin");
		petSetters.get(3).addPets(pet_11);
		Pet pet_12 = new Pet("Bambi", petSetters.get(3), "James");
		petSetters.get(3).addPets(pet_12);
		//add appointments
		m_sec.arrangeAppointment(pet_1, new Appointment(pet_1, petSetters.get(0), Appointment.AppointmentType.BATH, Util.StringToDate("2017-11-20 09:00:00"), Util.StringToDate("2017-11-20 10:00:00")));
		m_sec.arrangeAppointment(pet_1, new Appointment(pet_1, petSetters.get(0), Appointment.AppointmentType.HAIR_TRIMMER, Util.StringToDate("2017-11-20 10:00:00"), Util.StringToDate("2017-11-20 11:00:00")));
		m_sec.arrangeAppointment(pet_2, new Appointment(pet_2, petSetters.get(0), Appointment.AppointmentType.MASSAGE, Util.StringToDate("2017-11-20 11:00:00"), Util.StringToDate("2017-11-20 12:00:00")));
		m_sec.arrangeAppointment(pet_3, new Appointment(pet_3, petSetters.get(0), Appointment.AppointmentType.HAIR_TRIMMER, Util.StringToDate("2017-11-20 12:00:00"), Util.StringToDate("2017-11-20 13:00:00")));
		m_sec.arrangeAppointment(pet_3, new Appointment(pet_3, petSetters.get(0), Appointment.AppointmentType.MASSAGE, Util.StringToDate("2017-11-20 13:00:00"), Util.StringToDate("2017-11-20 14:00:00")));
		
		m_sec.arrangeAppointment(pet_4, new Appointment(pet_4, petSetters.get(1), Appointment.AppointmentType.TREATMENT, Util.StringToDate("2017-11-20 14:00:00"), Util.StringToDate("2017-11-20 15:00:00")));
		m_sec.arrangeAppointment(pet_5, new Appointment(pet_5, petSetters.get(1), Appointment.AppointmentType.TREATMENT, Util.StringToDate("2017-11-20 15:00:00"), Util.StringToDate("2017-11-20 16:00:00")));
		m_sec.arrangeAppointment(pet_5, new Appointment(pet_5, petSetters.get(1), Appointment.AppointmentType.HAIR_TRIMMER, Util.StringToDate("2017-11-20 16:00:00"), Util.StringToDate("2017-11-20 17:00:00")));
		m_sec.arrangeAppointment(pet_5, new Appointment(pet_5, petSetters.get(1), Appointment.AppointmentType.BATH, Util.StringToDate("2017-11-20 17:00:00"), Util.StringToDate("2017-11-20 18:00:00")));
		m_sec.arrangeAppointment(pet_6, new Appointment(pet_6, petSetters.get(1), Appointment.AppointmentType.COMBING, Util.StringToDate("2017-11-20 18:00:00"), Util.StringToDate("2017-11-20 19:00:00")));
		m_sec.arrangeAppointment(pet_7, new Appointment(pet_7, petSetters.get(1), Appointment.AppointmentType.BATH, Util.StringToDate("2017-11-21 09:00:00"), Util.StringToDate("2017-11-21 10:00:00")));
		m_sec.arrangeAppointment(pet_8, new Appointment(pet_8, petSetters.get(2), Appointment.AppointmentType.BATH, Util.StringToDate("2017-11-21 10:00:00"), Util.StringToDate("2017-11-21 11:00:00")));
		m_sec.arrangeAppointment(pet_8, new Appointment(pet_8, petSetters.get(2), Appointment.AppointmentType.MASSAGE, Util.StringToDate("2017-11-21 11:00:00"), Util.StringToDate("2017-11-21 12:00:00")));
		m_sec.arrangeAppointment(pet_9, new Appointment(pet_9, petSetters.get(2), Appointment.AppointmentType.HAIR_TRIMMER, Util.StringToDate("2017-11-21 12:00:00"), Util.StringToDate("2017-11-21 13:00:00")));
		m_sec.arrangeAppointment(pet_9, new Appointment(pet_9, petSetters.get(2), Appointment.AppointmentType.BATH, Util.StringToDate("2017-11-21 13:00:00"), Util.StringToDate("2017-11-21 14:00:00")));
		m_sec.arrangeAppointment(pet_10, new Appointment(pet_10, petSetters.get(3), Appointment.AppointmentType.TREATMENT, Util.StringToDate("2017-11-21 14:00:00"), Util.StringToDate("2017-11-21 15:00:00")));
		m_sec.arrangeAppointment(pet_11, new Appointment(pet_11, petSetters.get(3), Appointment.AppointmentType.BATH, Util.StringToDate("2017-11-21 15:00:00"), Util.StringToDate("2017-11-21 16:00:00")));
		m_sec.arrangeAppointment(pet_12, new Appointment(pet_12, petSetters.get(3), Appointment.AppointmentType.HAIR_TRIMMER, Util.StringToDate("2017-11-21 16:00:00"), Util.StringToDate("2017-11-21 17:00:00")));
		m_sec.arrangeAppointment(pet_12, new Appointment(pet_12, petSetters.get(3), Appointment.AppointmentType.BATH, Util.StringToDate("2017-11-21 17:00:00"), Util.StringToDate("2017-11-21 18:00:00")));
	}
	
	//arrange an appointment for a pet
	private void arrageAppointment(Pet pet)
	{
		if (null == pet || null == pet.getPetSetter())
			return;
		System.out.println("1. BATH, 2. MASSAGE, 3. COMBING, 4. HAIR_TRIMMER, 5. TREATMENT");
		int nType = -1; 
		while (true)
		{
			System.out.print("Input the type of appointment you want to arrange: ");
			nType = m_scan.nextInt();
			if (0 >= nType || 6 <= nType)
				System.out.println("Type is invalid.");
			else
			{
				break;
			}
		}
		Appointment.AppointmentType enumAppType = Appointment.AppointmentType.BATH;
		//get the appointment according to the input
		switch (nType)
		{
		case 1:
			enumAppType = Appointment.AppointmentType.BATH;
			break;
		case 2:
			enumAppType = Appointment.AppointmentType.MASSAGE;
			break;
		case 3:
			enumAppType = Appointment.AppointmentType.COMBING;
			break;
		case 4:
			enumAppType = Appointment.AppointmentType.HAIR_TRIMMER;
			break;
		case 5:
			enumAppType = Appointment.AppointmentType.TREATMENT;
			break;
		default:
			enumAppType = Appointment.AppointmentType.BATH;
			break;
		}
		Appointment app = new Appointment(pet, pet.getPetSetter(), enumAppType, null, null);
		pet.getPetSetter().getAppController().arrangeAppointment(app);
		m_sec.arrangeAppointment(pet, app);
		System.out.println("Arranged an appointment for " + pet.getName() + " from " + Util.getDateString(app.getStart()) + " to " + Util.getDateString(app.getEnd()));
	}
	
	//change the status of an appointment
	private void changeStatus(Pet pet)
	{
		if (null == pet || null == pet.getPetSetter())
			return;
		AppointmentController ac = pet.getAppController();
		if (null == ac)
			return;
		int nIndex = 0;
		boolean bHasApp = false;
		System.out.println("Information of the available appointments are: ");
		for (Appointment a : ac.getAppointments())
		{
			if (Appointment.AppointmentStatus.FINISH != a.getStatus())
			{
				bHasApp = true;
				System.out.println(nIndex + ". " + a.getAppointmentInfo());
			}
			nIndex++;
		}
		if (false == bHasApp)
		{
			System.out.println("No Available Appointments");
		}
		else
		{
			System.out.print("Input your choice: ");
			int nChoice = m_scan.nextInt();
			Appointment app = ac.getAppointments().get(nChoice);
			if (null == app || Appointment.AppointmentStatus.FINISH == app.getStatus())
			{
				System.out.println("The appointment may be not exist or have been finished.");
			}
			else
			{
				System.out.println("1. CANCEL, 2. ATTEND, 3. MISS, 4. FINISH");
				System.out.print("Input the new status of the appointment: ");
				int nStatus = m_scan.nextInt();
				switch (nStatus)
				{
				case 1:
					app.updateStatus(Appointment.AppointmentStatus.CANCEL);
					System.out.println("The appointment status is changed to CANCEL successfully.");
					break;
				case 2:
					app.updateStatus(Appointment.AppointmentStatus.ATTEND);
					System.out.println("The appointment status is changed to ATTEND successfully.");
					break;
				case 3:
					app.updateStatus(Appointment.AppointmentStatus.MISS);
					System.out.println("The appointment status is changed to MISS successfully.");
					break;
				case 4:
					app.updateStatus(Appointment.AppointmentStatus.FINISH);
					System.out.println("The appointment status is changed to FINISH successfully.");
					break;
				default:
					System.out.println("Invalid status.");
					break;
				}
			}
		}
	}
	
	//change time of an appointment
	private void changeTime(Pet pet)
	{
		if (null == pet || null == pet.getPetSetter())
			return;
		//display the appoint which is in ATTEND state
		AppointmentController ac = pet.getAppController();
		if (null == ac)
			return;
		int nIndex = 0;
		boolean bHasApp = false;
		System.out.println("Information of the available appointments are: ");
		for (Appointment a : ac.getAppointments())
		{
			if (Appointment.AppointmentStatus.ATTEND == a.getStatus())
			{
				bHasApp = true;
				System.out.println(nIndex + ". " + a.getAppointmentInfo());
			}
			nIndex++;
		}
		if (false == bHasApp)
		{
			System.out.println("No Available Appointments");
		}
		else
		{
			System.out.print("Input your choice: ");
			int nChoice = m_scan.nextInt();
			//clear the last return in the scanner
			m_scan.nextLine();
			Appointment app = null;
			if (ac.getAppointments().size() <= nChoice
				|| null == (app = ac.getAppointments().get(nChoice)) 
				|| Appointment.AppointmentStatus.ATTEND != app.getStatus())
			{
				System.out.println("Invalid Choice.");
			}
			else
			{
				System.out.print("Input the start time <yyyy-MM-dd HH:mm:ss>: ");
				String strStart = m_scan.nextLine();
				Date dtStart = Util.StringToDate(strStart);
				if (null == dtStart)
				{
					System.out.println("Start time is invalid.\n");
					return;
				}
				System.out.print("Input the finish time <yyyy-MM-dd HH:mm:ss>: ");
				String strFin = m_scan.nextLine();
				Date dtFin = Util.StringToDate(strFin);
				if (null == dtFin)
				{
					System.out.println("Finish time is invalid.\n");
					return;
				}
				m_sec.removeAppointment(pet, app);
				//back up the time
				Date dtStartTmp = app.getStart(), dtFinTmp = app.getEnd();
				app.updateTime(dtStart, dtFin);
				if (m_sec.isAppointmentValid(pet, app))
				{
					System.out.println("Change time successfully!");
				}
				else
				{
					//restore the time
					app.updateTime(dtStartTmp, dtFinTmp);
					System.out.println("Change time failed!");
				}
				m_sec.arrangeAppointment(pet, app);
			}
		}
	}
	//do summary
	private void doSummary()
	{
		m_sec.doSummary();
	}
	
	//in our program, log in by pet name and owner name, return the pet
	private Pet login()
	{
		System.out.println("Welcome to the Pet Saloon, please login first.");
		System.out.println("----------------------------------------------");
		System.out.print("Input the name of the pet: ");
		String strPetName = m_scan.nextLine();
		System.out.print("Input the name of the owner: ");
		String strOwnerName = m_scan.nextLine();
		return m_sec.login(strPetName, strOwnerName);
	}
	//show main menu
	private void showMainMenu()
	{
		System.out.println("============== Main Menu ==============");
		System.out.println("=   1. Arrange an appointment         =");
		System.out.println("=   2. Change appointment time        =");
		System.out.println("=   3. Change appointment status      =");
		System.out.println("=   4. Show summary                   =");
		System.out.println("=   5. Exit the system                =");
		System.out.println("=======================================");
		System.out.print("Input your option: ");
	}
	
	//start the system
	public void start()
	{
		//initialize the data first
		initData();
		//login
		Pet pet = login();
		if (null == pet)
		{
			System.out.println("Login error, pet name or owner name may be error!");
			return;
		}
		boolean bFin = false;
		int nOption = 0;
		while (!bFin)
		{
			showMainMenu();
			nOption = m_scan.nextInt();
			switch (nOption)
			{
			case 1:
				arrageAppointment(pet);
				break;
			case 2:
				changeTime(pet);
				break;
			case 3:
				changeStatus(pet);
				break;
			case 4:
				doSummary();
				break;
			case 5:
				bFin = true;
				break;
			default:
				System.out.println("Invalid option.");
				break;
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MainInterface mi = new MainInterface();
		mi.start();
	}

}
