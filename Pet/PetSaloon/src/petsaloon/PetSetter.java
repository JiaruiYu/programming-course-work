package petsaloon;
import java.util.ArrayList;

public class PetSetter {
	private String m_strName;
	private ArrayList<Pet> m_lstPets;
	//a pet setter have different appointments with different pets
	private AppointmentController m_appController;
	public PetSetter(String strName)
	{
		m_strName = strName;
		m_appController = new AppointmentController();
		m_lstPets = new ArrayList<Pet>();
	}
	public String getName()
	{
		return m_strName;
	}
	//need to check if the pet with the same name and same owner already exist
	public void addPets(Pet pet)
	{
		m_lstPets.add(pet);
	}
	//add an appointment to the appointment controller
	public boolean addAppointment(Appointment app)
	{
		return m_appController.addAppointment(app);
	}
	//get the pet by name and the owner information
	public Pet getPetByNameAndOwner(String strName, String strOwner)
	{
		for (Pet pet : m_lstPets)
		{
			if (pet.getName().equals(strName) && pet.getOwner().equals(strOwner))
				return pet;
		}
		return null;
	}
	//check if appointment is valid
	public boolean isAppointmentValid(Appointment app)
	{
		return m_appController.isAppointmentValid(app);
	}
	//remove appointment
	public void removeAppointment(Appointment app)
	{
		m_appController.getAppointments().remove(app);
	}
	//get the appointment controller
	public AppointmentController getAppController()
	{
		return m_appController;
	}
}
