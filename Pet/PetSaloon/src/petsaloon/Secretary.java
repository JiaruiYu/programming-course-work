package petsaloon;
import java.util.ArrayList;

public class Secretary {
	//Secretary contains the information of a lot of petSetters
	private ArrayList<PetSetter> m_lstPetSetters;
	//only one secretary, use static 
	private static Secretary s_sec = null;
	//we need to use singleton
	private Secretary()
	{
		m_lstPetSetters = new ArrayList<PetSetter>();
	}
	//get the only one secretary
	public static Secretary getInstance()
	{
		if (null == s_sec)
			s_sec = new Secretary();
		return s_sec;
	}
	
	//get the pet setters
	public ArrayList<PetSetter> getPetSetters()
	{
		return m_lstPetSetters;
	}
	
	//add pet setter, need to check if the setter already exist
	public boolean addPetSetter(PetSetter petSetter)
	{
		if (null == petSetter)
			return false;
		for (PetSetter curPetSetter : m_lstPetSetters)
		{
			if (curPetSetter.getName().equals(petSetter.getName()))
				return false;
		}
		m_lstPetSetters.add(petSetter);
		return true;
	}
	//arrange an appointment for a pet
	public void arrangeAppointment(Pet pet, Appointment app)
	{
		if (null == pet || null == app)
			return;
		PetSetter petSetter = pet.getPetSetter();
		if (null == petSetter)
			return;
		pet.addAppointment(app);
		petSetter.addAppointment(app);
	}
	
	public void removeAppointment(Pet pet, Appointment app)
	{
		if (null == pet || null == app)
			return;
		PetSetter petSetter = pet.getPetSetter();
		if (null == petSetter)
			return;
		pet.removeAppointment(app);
		petSetter.removeAppointment(app);
	}
	//check if the appointment time is valid
	public boolean isAppointmentValid(Pet pet, Appointment app)
	{
		if (null == pet || null == app)
			return false;
		PetSetter petSetter = pet.getPetSetter();
		if (null == petSetter)
			return false;
		return pet.isAppointmentValid(app) && petSetter.isAppointmentValid(app);
	}
	//do summary
	public void doSummary()
	{
		int nCancel = 0, nAttend = 0, nMiss = 0, nFin = 0;
		for (PetSetter petSetter : m_lstPetSetters)
		{
			if (null == petSetter)
				continue;
			for (Appointment app : petSetter.getAppController().getAppointments())
			{
				if (null == app)
					continue;
				Appointment.AppointmentStatus appStatus = app.getStatus();
				if (Appointment.AppointmentStatus.ATTEND == appStatus)
				{
					nAttend++;
				}
				else if (Appointment.AppointmentStatus.CANCEL == appStatus)
				{
					nCancel++;
				}
				else if (Appointment.AppointmentStatus.MISS == appStatus)
				{
					nMiss++;
				}
				else
				{
					nFin++;
				}
			}
		}
		System.out.println("-----------------------------------");
		System.out.println("Summary of the total appointments: ");
		System.out.println("ATTEND: " + nAttend);
		System.out.println("CANCEL: " + nCancel);
		System.out.println("MISS: " + nMiss);
		System.out.println("FINISH: " + nFin);
		System.out.println("-----------------------------------");
	}
	
	//log in by pet name and owner name
	public Pet login(String strPetName, String strOwnerName)
	{
		Pet pet = null;
		for (PetSetter petSetter : m_lstPetSetters)
		{
			if (null == petSetter)
				continue;
			pet = petSetter.getPetByNameAndOwner(strPetName, strOwnerName);
			if (null != pet)
				return pet;
		}
		return null;
	}
}
