package petsaloon;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AppointmentController {
	private ArrayList<Appointment> m_lstAppointments;
	public AppointmentController()
	{
		m_lstAppointments = new ArrayList<Appointment>();
	}
	//get the place to insert the appointment
	public int getAppointentPlace(Appointment app)
	{
		if (null == app)
			return -1;
		int nIndex = 0;
		for (nIndex = 0; nIndex < m_lstAppointments.size(); nIndex++)
		{
			//if the appointment is canceled, continue
			if (Appointment.AppointmentStatus.CANCEL == app.getStatus())
				continue;
			if (app.getStart().before(m_lstAppointments.get(nIndex).getStart()))
			{
				if (app.getEnd().before(m_lstAppointments.get(nIndex).getStart()))
				{
					return nIndex;
				}
				return -1;
			}
		}
		return nIndex;
	}
	//add an appointement
	public boolean addAppointment(Appointment app)
	{
		int nPlace = getAppointentPlace(app);
		if (-1 == nPlace)
			return false;
		m_lstAppointments.add(nPlace, app);
		return true;
	}
	public boolean isAppointmentValid(Appointment app)
	{
		return -1 != getAppointentPlace(app);
	}
	public ArrayList<Appointment> getAppointments()
	{
		return m_lstAppointments;
	}
	//get a valid time according to the appointment time list
	public void arrangeAppointment(Appointment app)
	{
		if (null == app)
			return;
		int nSize = m_lstAppointments.size();
		Date startDate = null, endDate = null;
		//if there is no arrangements currently, we arrange 3 days later from now
		Calendar calendar = Calendar.getInstance();
                Date curDate = new Date();
		calendar.setTime(curDate);
		if (0 == nSize)
		{
			calendar.add(calendar.DAY_OF_MONTH, 3);
			//start is 9:00
			calendar.set(calendar.HOUR_OF_DAY, 9);
			startDate = calendar.getTime();
		}
		else
		{
			startDate = m_lstAppointments.get(nSize - 1).getEnd();
                        if (curDate.after(startDate))
                        {
                            calendar.add(calendar.DAY_OF_MONTH, 1);
                            //start is 9:00
                            calendar.set(calendar.HOUR_OF_DAY, 9);
                            startDate = calendar.getTime();
                        }
		}
		calendar.setTime(startDate);
		calendar.add(calendar.HOUR_OF_DAY, 1);
		endDate = calendar.getTime();
		app.updateTime(startDate, endDate);
	}
}
