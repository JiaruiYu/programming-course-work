package petsaloon;
import java.util.ArrayList;

public class Pet {
	//the name and the owner identifies a unique pet
	private String m_strPetName;
	private PetSetter m_petSetter;
	private String m_strOwnerInfo;
	//a pet may have different appointments with a pet setter
	private AppointmentController m_appController;
	//the construct function of the 
	public Pet(String strPetName, PetSetter petSetter, String ownerContact)
	{
		m_strPetName = strPetName;
		m_petSetter = petSetter;
		m_strOwnerInfo = ownerContact;
		m_appController = new AppointmentController();
	}
	//get the owner information of the pet
	public String getOwner()
	{
		return m_strOwnerInfo;
	}
	//get the name of the pet
	public String getName()
	{
		return m_strPetName;
	}
	//get the pet setter
	public PetSetter getPetSetter()
	{
		return m_petSetter;
	}
	public boolean addAppointment(Appointment app)
	{
		return m_appController.addAppointment(app);
	}
	public boolean isAppointmentValid(Appointment app)
	{
		return m_appController.isAppointmentValid(app);
	}
	//remove appointment
	public void removeAppointment(Appointment app)
	{
		m_appController.getAppointments().remove(app);
	}
	public AppointmentController getAppController()
	{
		return m_appController;
	}
}
