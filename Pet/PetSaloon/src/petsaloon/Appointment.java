package petsaloon;
import java.util.Date;

public class Appointment {
	public enum AppointmentType
	{
		BATH, MASSAGE, COMBING, HAIR_TRIMMER, TREATMENT
	}
	public enum AppointmentStatus
	{
		CANCEL, ATTEND, MISS, FINISH
	}
	private AppointmentType m_type;
	private AppointmentStatus m_status;
	private Pet m_pet;
	private PetSetter m_petSetter;
	private Date m_dtStart, m_dtEnd;
	public Appointment(Pet pet, PetSetter petSetter, AppointmentType type, Date dtStart, Date dtEnd)
	{
		m_pet = pet;
		m_petSetter = petSetter;
		m_type = type;
		//initialize the status with ATTEND
		m_status = AppointmentStatus.ATTEND;
		m_dtStart = dtStart;
		m_dtEnd = dtEnd;
	}
	public String getAppointmentInfo()
	{
		//pet name --- setter name
		String strInfo = "Pet Name: " + m_pet.getName() + "\tPet Setter: " + m_petSetter.getName();
		strInfo += "\tFrom: " + Util.getDateString(m_dtStart) + "\tTo: " + Util.getDateString(m_dtEnd);
		return strInfo;
	}
	//update the status of the appointment
	public void updateStatus(AppointmentStatus status)
	{
		m_status = status;
	}
	public AppointmentStatus getStatus()
	{
		return m_status;
	}
	public AppointmentType getType()
	{
		return m_type;
	}
	public Pet getPet()
	{
		return m_pet;
	}
	public PetSetter getPetSetter()
	{
		return m_petSetter;
	}
	
	public void updateTime(Date dtStart, Date dtEnd)
	{
		m_dtStart = dtStart;
		m_dtEnd = dtEnd;
	}
	
	public Date getStart()
	{
		return m_dtStart;
	}
	
	public Date getEnd()
	{
		return m_dtEnd;
	}
	
	public boolean isTimeConflict(Date dtTime)
	{
		return dtTime.after(m_dtStart) && dtTime.before(m_dtEnd);
 	}
}
