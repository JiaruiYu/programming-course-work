import static org.junit.Assert.*;

import org.junit.Test;
import petsaloon.*;

public class PetSaloonTest {
	// set up data
	MainInterface mainInt = new MainInterface();

	public PetSaloonTest() {
		mainInt.initData();
	}

	// test only one instance in the system
	@Test
	public void testSecretary() {
		assertEquals(mainInt.getSecretary(), Secretary.getInstance());
	}

	// test login
	@Test
	public void testLogin() {
		Secretary sec = mainInt.getSecretary();
		assertEquals(null, sec.login("Doggy", "Bill"));
		assertNotEquals(null, sec.login("Cookie", "Jack"));
	}

	// test if we can add a pet setter
	@Test
	public void testAddPetSetter() {
		// a fail case
		Secretary sec = mainInt.getSecretary();
		int nPetNum = sec.getPetSetters().size();
		PetSetter petSetter = new PetSetter("pet's home #1");
		sec.addPetSetter(petSetter);
		assertEquals(nPetNum, sec.getPetSetters().size());
		// a success case
		petSetter = new PetSetter("pet's home #5");
		sec.addPetSetter(petSetter);
		assertEquals(1 + nPetNum, sec.getPetSetters().size());
	}

	// test if we can arrange an appointment
	@Test
	public void testArrangeAppointment() {
		Secretary sec = mainInt.getSecretary();
		Pet pet = sec.login("Cookie", "Jack");
		int nAppNum = pet.getAppController().getAppointments().size();
		Appointment app = new Appointment(pet, pet.getPetSetter(), 
				Appointment.AppointmentType.BATH, null, null);
		pet.getPetSetter().getAppController().arrangeAppointment(app);
		sec.arrangeAppointment(pet, app);
		assertEquals(1 + nAppNum, pet.getAppController().getAppointments().size());
	}

	// test if we can arrange an appointment
	@Test
	public void testRemoveAppointment() {
		Secretary sec = mainInt.getSecretary();
		Pet pet = sec.login("Cookie", "Jack");
		int nAppNum = pet.getAppController().getAppointments().size();
		Appointment app = pet.getAppController().getAppointments().get(0);
		sec.removeAppointment(pet, app);
		assertEquals(nAppNum - 1, pet.getAppController().getAppointments().size());
	}
}
